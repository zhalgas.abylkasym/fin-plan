from fastapi import FastAPI, Form, Request
from fastapi.templating import Jinja2Templates

app = FastAPI()

# Templates configuration
templates = Jinja2Templates(directory="templates")


@app.get("/")
def read_root(request: Request):
    return templates.TemplateResponse("index.html", {"request": request})

@app.post("/calculate")
def calculate_budget(
    income: float = Form(...),
    expenses: float = Form(...),
    savings: float = Form(...),
):
    # Simple financial planning logic
    budget = income - expenses - savings

    # Mock data for displaying goals
    goals = [
        {"title": "Save for a Vacation", "amount": 500},
        {"title": "Emergency Fund", "amount": 1000},
        # Add more goals as needed
    ]

    return {"budget": budget, "goals": goals}